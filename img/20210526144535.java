package ce.mes.dfd.iicounsumer.Base.BusinessLogic;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;

import com.ce.mes.base.connection.ConnectionUtil;

import ce.mes.dfd.iicounsumer.Base.common.Base64;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.json.JsonSerializer;
import java.util.Base64;

public class BusinessHandle {

	private static Log log = LogFactory.getLog(BusinessHandle.class);

	public List<Map<String, Object>> createBatteryPack(String messageNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();

		List<Map<String, Object>> packList = new ArrayList<>();// 电池包

		try {
			/*
			 * 查询定 电池包中的模块及模块的电芯信息，并按 Map<String,Map<String, List<String>>> 结构存放
			 * 方便处理电池包的模块信息(接口第二层)时直接取用
			 */
			StringBuffer batterySql = new StringBuffer();
			batterySql.append("select t3.BoxCode boxNo, t1.BoxCode modelNo ,t1.MaterialNo,t2.UnitNo,t2.DxNo ");
			batterySql.append("from  UDT_Process_BoxRefParts t1 ");
			batterySql.append("join UDT_Process_UnitRefDx t2  on t1.MaterialNo = t2.UnitNo ");
			batterySql.append("join UDT_Process_BoxRefParts t3 on t3.MaterialNo = t1.BoxCode ");
			batterySql.append("where t1.ProductType = 'M' AND ISNULL(t1.Releve,'0')='0' ");
			batterySql.append(
					"	and exists (select MaterialNo from UDT_Process_BoxRefParts t where t.MaterialNo = t1.BoxCode  ");
			batterySql.append("and t.BoxCode in (select Mat_No from UDT_IS_Send_ToCustomer where msgHeaderNo = ?)) ");
			batterySql.append("order by boxNo,modelNo ");
			PreparedStatement batteryPrep = conn.prepareStatement(batterySql.toString());
			batteryPrep.setString(1, messageNo);
			ResultSet batteryRest = batteryPrep.executeQuery();

			Map<String, Map<String, List<String>>> boxMap = new HashMap<>();

			while (batteryRest.next()) {
				String boxNo = batteryRest.getString(1);
				String moduleNo = batteryRest.getString(2);
				String batteryNo = batteryRest.getString(5);

				if (boxMap.get(boxNo) != null) {
					if (boxMap.get(boxNo).get(moduleNo) != null) {
						boxMap.get(boxNo).get(moduleNo).add(batteryNo);
						// System.out.println("batteryNo : "+batteryNo);
					} else {
						// System.out.println("moduleNo : ==========
						// "+moduleNo);
						List<String> batteryList = new ArrayList<>();
						batteryList.add(batteryNo);
						Map<String, List<String>> modelMap = boxMap.get(boxNo);

						modelMap.put(moduleNo, batteryList);
						// System.out.println("batteryNo : "+batteryNo);
					}

				} else {
					List<String> batteryList = new ArrayList<>();
					batteryList.add(batteryNo);
					Map<String, List<String>> modelMap = new HashMap<>();
					modelMap.put(moduleNo, batteryList);
					boxMap.put(boxNo, modelMap);
					// System.out.println("boxNo : ======== "+boxNo);
					// System.out.println("moduleNo : ========== "+moduleNo);
					// System.out.println("batteryNo : "+batteryNo);
				}
			}

			/*
			 * 查询电池包和电池包模块信息(电池包编码，模块编号、模块产品型号、模块工单号、电芯编号，电芯产品型号) 并以Map<String,
			 * List<Map<String, Object>>> 数据结构存放，方便处理电池包信息（接口第一层）时查询
			 */
			StringBuffer moduleSql = new StringBuffer();
			moduleSql.append("select t1.BoxCode,t1.MaterialNo modelNo,t1.MaterialCode modelCode, ");
			moduleSql.append(
					"(select t.Product_Model from UDT_Products_Base_Exten t where t.Prod_Code = t1.MaterialCode ) as mpModel,");
			moduleSql.append("t2.Po_No,t3.Materialcode batteryCode,t5.Product_Model bpModel ");
			moduleSql.append("from  UDT_Process_BoxRefParts t1  ");
			moduleSql.append("join UDT_MAT t2  on t1.MaterialNo = t2.Mat_No ");
			moduleSql.append("join UDT_Base_BOM t3  on t2.Po_No = t3.OrderNumber and t3.RecoilSign = '1' ");
			moduleSql.append("join UDT_Products_Base_Exten t5 on t5.Prod_Code = t3.Materialcode ");
			moduleSql.append("where t1.ProductType = 'B' and isnull(Releve,'0')='0'  and  t1.boxCode in ");
			moduleSql.append("(select Mat_No from UDT_IS_Send_ToCustomer where msgHeaderNo = ? )");

			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			modulePrep.setString(1, messageNo);
			ResultSet moduleRest = modulePrep.executeQuery();

			// Map<String, List<Map<String, Object>>> key 为 电池包号
			// List<Map<String, Object>> key 为模块号信息（接口第二层内容）
			Map<String, List<Map<String, Object>>> boxModelMap = new HashMap<>();

			while (moduleRest.next()) {
				String boxNo = moduleRest.getString(1);
				String moduleNo = moduleRest.getString(2);
				if (boxModelMap.get(boxNo) != null) {
					Map<String, Object> moudelMsg = new HashMap<>();
					moudelMsg.put("code", moduleNo);
					moudelMsg.put("cellList", boxMap.get(boxNo).get(moduleNo));// 电芯信息
					moudelMsg.put("modelId", moduleRest.getString(4));
					moudelMsg.put("cellModelId", moduleRest.getString(7));
					boxModelMap.get(boxNo).add(moudelMsg);
				} else {
					Map<String, Object> moduleMsg = new HashMap<>();
					moduleMsg.put("code", moduleNo);
					moduleMsg.put("cellList", boxMap.get(boxNo).get(moduleNo));// 电芯信息
					moduleMsg.put("modelId", moduleRest.getString(4));
					moduleMsg.put("cellModelId", moduleRest.getString(7));
					List<Map<String, Object>> moduleMapList = new ArrayList<>();
					moduleMapList.add(moduleMsg);
					boxModelMap.put(boxNo, moduleMapList);
				}
				// log.info(boxNo+"\n = moduleNo:"+moduleNo+" :
				// "+boxMap.get(boxNo).get(moduleNo).size());

			}

			// 读取电池包信息
			StringBuffer boxSql = new StringBuffer();
			boxSql.append("select t1.Po_No,t1.Mat_No,t2.Customer_PurchaseNo,t2.ProductCode,t3.Product_Model ");
			boxSql.append(" from UDT_IS_Send_ToCustomer t1 ");
			boxSql.append("left join UDT_Base_Orders t2  on t1.Po_No = t2.OrderNumber ");
			boxSql.append("left join UDT_Products_Base_Exten t3 on t2.ProductCode = t3.Prod_Code");
			boxSql.append("	where t1.msgHeaderNo = ? ");

			PreparedStatement boxPrep = conn.prepareStatement(boxSql.toString());
			boxPrep.setString(1, messageNo);
			ResultSet boxRest = boxPrep.executeQuery();

			while (boxRest.next()) {
				Map<String, Object> boxPack = new HashMap<>();
				String boxNo = boxRest.getString(2);
				boxPack.put("code", boxNo);
				boxPack.put("modelId", boxRest.getString(5));
				boxPack.put("orderNo", boxRest.getString(3));
				boxPack.put("moduleList", boxModelMap.get(boxNo));// 获取模块的内容
				packList.add(boxPack);
				log.info("boxNo : " + boxNo + " === moduleSize :" + boxModelMap.get(boxNo).size());
			}

		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return packList;
	}

	public HttpResponse send(String url, Object data, String token, String key) throws Exception {
		try {
			Map<String, Object> requestMap = new HashMap<String, Object>();
			String requestMsg = new JsonSerializer().deep(true).serialize(data);
			String enStr = encrypt(requestMsg, key);
			requestMap.put("requestMsg", enStr);
			requestMap.put("timestamp", System.currentTimeMillis());

			String sign = byteArrayToHexString(encryptHMAC(enStr.getBytes(), key));
			requestMap.put("sign", sign);
			HttpResponse response = getRequest(url, token, requestMap).send().unzip().unzip();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);

		}
		return null;
	}

	public static String encrypt(String content, String password) throws Exception {
		// try {
		if ("".equals(content) || content == "") {
			return "";
		}
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
		secureRandom.setSeed(password.getBytes());
		kgen.init(128, secureRandom);
		SecretKey secretKey = kgen.generateKey();
		byte[] enCodeFormat = secretKey.getEncoded();
		SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
//		BASE64Encoder coder = new BASE64Encoder();
		coder.encode(enCodeFormat);
		Cipher cipher = Cipher.getInstance("AES");
		byte[] byteContent = content.getBytes("utf-8");
		cipher.init(1, key);
		byte[] result = cipher.doFinal(byteContent);
		String str = Base64.encode(result);
		return str;
		// } catch (NoSuchAlgorithmException var13) {
		// ;
		// } catch (NoSuchPaddingException var14) {
		// ;
		// } catch (InvalidKeyException var15) {
		// ;
		// } catch (UnsupportedEncodingException var16) {
		// ;
		// } catch (IllegalBlockSizeException var17) {
		// ;
		// } catch (BadPaddingException var18) {
		// ;
		// }
		// return null;
	}

	public static String encrypt1(String content, String password) {
		try {
			if (StringUtils.isEmpty(content))
				return "";
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			random.setSeed(password.getBytes());
			kgen.init(128, random);
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			byte[] byteContent = content.getBytes("utf-8");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] result = cipher.doFinal(byteContent);
			String str = Base64.encode(result);
			return str;
		} catch (NoSuchAlgorithmException e) {
			// e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// e.printStackTrace();
		} catch (InvalidKeyException e) {
			// e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// e.printStackTrace();
		} catch (BadPaddingException e) {
			// e.printStackTrace();
		}
		return null;
	}

	public static byte[] encryptHMAC(byte[] data, String key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key.getBytes(), "HmacMD5");
		Mac mac = Mac.getInstance(secretKey.getAlgorithm());
		mac.init(secretKey);
		return mac.doFinal(data);
	}

	public static String encodeMD5(String str) {
		String strDigest = "";
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] data = md5.digest(str.getBytes("utf-8"));
			strDigest = bytesToHexString(data);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return strDigest;
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	public static String byteArrayToHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		byte[] var2 = bytes;
		int var3 = bytes.length;
		for (int var4 = 0; var4 < var3; ++var4) {
			byte aByte = var2[var4];
			int v = aByte & 255;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString();
	}

	public HttpRequest getRequest(String url, String accessToken, Map<String, Object> params) {
		HttpRequest request = HttpRequest.post(url);
		request.header("Authorization", "Bearer " + accessToken);
		request.header("Content-Type", "application/json; charset=utf-8");
		request.acceptEncoding("gzip");
		request.body((new JsonSerializer()).serialize(params));
		return request;
	}

	@SuppressWarnings("rawtypes")
	public Map<String, String> parseRsponseBody(HttpResponse response, String key) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Map map = mapper.readValue(response.body(), Map.class);

		Map<String, String> resultMap = new HashMap<>();
		resultMap.put("code", map.get("code").toString());
		if (isNotEmpty(map.get("data"))) {
			resultMap.put("errorMessages",
					new JSONObject(decrypt(map.get("data").toString(), key)).getString("errorMessages"));
		}
		return resultMap;
	}

	public static boolean isNotEmpty(Object o) {
		if (o == null) {
			return false;
		}
		if ("".equals(FilterNull(o.toString()))) {
			return false;
		} else {
			return true;
		}
	}

	public static String FilterNull(Object o) {
		return o != null && !"null".equals(o.toString()) ? o.toString().trim() : "";
	}

	public static String decrypt(String str, String password) {
		try {
			byte[] content = Base64.decode(str);
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(password.getBytes());
			kgen.init(128, secureRandom);
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(2, key);
			byte[] result = cipher.doFinal(content);
			return new String(result, "UTF-8");
		} catch (NoSuchAlgorithmException var10) {
			;
		} catch (NoSuchPaddingException var11) {
			;
		} catch (InvalidKeyException var12) {
			;
		} catch (IllegalBlockSizeException var13) {
			;
		} catch (BadPaddingException var14) {
			;
		} catch (Exception var15) {
			;
		}
		return "";
	}

	public List<Map<String, Object>> createBatteryPackQr(String messageNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();
		List<Map<String, Object>> packList = new ArrayList<>();// 电池包
		try {
			/*
			 * 查询定 电池包中的模块及模块的电芯信息，并按 Map<String,Map<String, List<String>>> 结构存放
			 * 方便处理电池包的模块信息(接口第二层)时直接取用
			 */
			StringBuffer moduleSql = new StringBuffer();
			moduleSql.append(
					"SELECT t3.BoxCode boxNo, ( SELECT t.Product_Model FROM UDT_Products_Base_Exten t WHERE t.");
			moduleSql.append("Prod_Code = t8.ProductCode ) AS Product_Model,    t1.BoxCode modelNo,   ( SELECT t.");
			moduleSql.append("Product_Model FROM UDT_Products_Base_Exten t WHERE t.Prod_Code = t10.ProductCode ) AS ");
			moduleSql.append("mpModel,    t1.MaterialNo,  t2.UnitNo,  t2.DxNo, t6.Product_Model bpModel, t8.");
			moduleSql
					.append("Customer_PurchaseNo, t8.qty ,( SELECT TOP 1 Send_Base FROM UDT_IS_Send_ToCustomer WHERE msgHeaderNo = '"
							+ messageNo
							+ "' ) as Send_Base FROM UDT_Process_BoxRefParts t1 JOIN UDT_Process_UnitRefDx t2 ");
			moduleSql.append(
					"ON t1.MaterialNo = t2.UnitNo JOIN UDT_Process_BoxRefParts t3 ON t3.MaterialNo = t1.BoxCode ");
			moduleSql.append("JOIN UDT_MAT t4 ON t1.BoxCode = t4.Mat_No JOIN UDT_Base_BOM t5 ON t4.Po_No = t5.");
			moduleSql.append(
					"OrderNumber AND t5.RecoilSign = '1' JOIN UDT_Products_Base_Exten t6 ON t6.Prod_Code = t5.");
			moduleSql.append(
					"Materialcode join  UDT_MAT t7 ON t3.BoxCode = t7.Mat_No left join UDT_Base_Orders t8  on ");
			moduleSql.append("t7.Po_No = t8.OrderNumber join  UDT_MAT t9 ON t1.BoxCode = t9.Mat_No left join ");
			moduleSql.append("UDT_Base_Orders t10  on t9.Po_No = t10.OrderNumber WHERE t1.ProductType = 'M' ");
			moduleSql.append("AND ISNULL( t1.Releve, '0' ) = '0' AND EXISTS ( SELECT MaterialNo FROM ");
			moduleSql.append("		UDT_Process_BoxRefParts t  WHERE t.MaterialNo = t1.BoxCode  ");
			moduleSql.append("AND t.BoxCode IN ( SELECT Mat_No FROM UDT_IS_Send_ToCustomer WHERE msgHeaderNo = '"
					+ messageNo + "' )) ");
			moduleSql.append("ORDER BY boxNo,modelNo");
			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			ResultSet moduleRest = modulePrep.executeQuery();

			/*
			 * 查询电池包和电池包模块信息(电池包编码，模块编号、模块产品型号、模块工单号、电芯编号，电芯产品型号) 并以Map<String,
			 * List<Map<String, Object>>> 数据结构存放，方便处理电池包信息（接口第一层）时查询
			 */
			while (moduleRest.next()) {
				String Cgdd_Num = moduleRest.getString(9);// 订单号
				String Send_Base = moduleRest.getString(11); // 发往地
				String Chn_Sl = moduleRest.getString(10);// 订单所含电池包总数
				String Box_Num = "1"; // 箱号
				// String Chn_Bm = moduleRest.getString(1);//所属储能装置编码
				// //接口修改已删除字段
				// String Chn_Xh =
				// moduleRest.getString(2);//moduleRest.getString(2);//"113AAM";
				// //所属储能装置型号//接口修改已删除字段
				String Dcb_Bm = moduleRest.getString(1); // 电池包编码
				String Dcb_Xh = moduleRest.getString(2);// moduleRest.getString(2);//"113AAM";
														// //电池包规格
				String Dcmk_Bm = moduleRest.getString(3);// 电池模块编码
				String Dcmk_Xh = moduleRest.getString(4);// moduleRest.getString(4);//"2P10S-PSP12161227-55Ah";//电池模块规格
				String Dcdt_Bm = moduleRest.getString(7);// 电芯编码
				String Dcdt_Xh = moduleRest.getString(8);// 电芯规格

				char ss = Dcdt_Xh.substring(Dcdt_Xh.length() - 1).charAt(0);
				if (Character.isUpperCase(ss))
					Dcdt_Xh = Dcdt_Xh.substring(0, Dcdt_Xh.length() - 1) + String.valueOf(ss).toLowerCase();

				Map<String, Object> moudelMsg = new LinkedHashMap<>();

				String ReturnPa = Verification(Cgdd_Num, Send_Base, Chn_Sl, Box_Num, "", "", Dcb_Bm, Dcb_Xh, Dcmk_Bm,
						Dcmk_Xh, Dcdt_Bm, Dcdt_Xh);
				if (!"".equals(ReturnPa)) {
					moudelMsg.put("Cgdd_key", ReturnPa);
					packList.add(moudelMsg);
					return packList;
				}
				moudelMsg.put("Cgdd_Num", Cgdd_Num);
				moudelMsg.put("Send_Base", Send_Base);
				moudelMsg.put("Chn_Sl", Chn_Sl);
				// moudelMsg.put("Chn_Xh", Chn_Xh);
				// moudelMsg.put("Chn_Bm", Chn_Bm);
				moudelMsg.put("Box_Num", Box_Num);
				moudelMsg.put("Dcb_Gg", Dcb_Xh);
				moudelMsg.put("Dcb_Bm", Dcb_Bm);
				moudelMsg.put("Dcmk_Gg", Dcmk_Xh);
				moudelMsg.put("Dcmk_Bm", Dcmk_Bm);
				moudelMsg.put("Dcdt_Gg", Dcdt_Xh);
				moudelMsg.put("Dcdt_Bm", Dcdt_Bm);
				packList.add(moudelMsg);
			}
		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return packList;
	}

	public String sendQr(String url, Object data, String token, String key) throws Exception {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Accept", "application/json");
		try {
			Map<String, Object> requestMap = new LinkedHashMap<String, Object>();
			requestMap.put("UserName", token);
			requestMap.put("Password", key);
			requestMap.put("BatteryDetial", data);
			String response = getRequestqr(url, token, requestMap);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	public Map<String, Object> createReqHead(String authKey, String authPWD) throws Exception {
		UUID uuid = UUID.randomUUID();
		String encryptMode = "AES";
		String reqGUID = uuid.toString();
		String reqSign = encodeMD5(authKey + "|" + authPWD + "|" + reqGUID);
		Map<String, Object> reqHead = new LinkedHashMap<String, Object>();
		reqHead.put("authKey", authKey);
		reqHead.put("encryptMode", encryptMode);
		reqHead.put("reqGUID", reqGUID);
		reqHead.put("reqSign", reqSign);

		return reqHead;
	}

	public Map<String, Object> createCompanyInfo() throws Exception {

		String type = "1";// 电池厂商类型
		String creditCode = "914108035664698524"; // 统一社会信用代码
		String code = "012";// 企业编码
		String name = "多氟多新能源科技有限公司"; // 企业名称
		String linkMan = "刘兴福"; // 联系人
		String phoneNum = "0391-2956011";// 联系电话
		String email = "zqf@dfdxny.com";// 联系邮箱
		String address = "河南省焦作市工业产业集聚区西部园区新园路北侧标准化厂房区";// 注册地址

		Map<String, Object> companyInfo = new LinkedHashMap<>();
		companyInfo.put("type", type);
		companyInfo.put("creditCode", creditCode);
		companyInfo.put("code", code);
		companyInfo.put("name", name);
		companyInfo.put("linkMan", linkMan);
		companyInfo.put("phoneNum", phoneNum);
		companyInfo.put("email", email);
		companyInfo.put("address", address);

		return companyInfo;
	}

	public Map<String, Object> createPackRecord() throws Exception {

		String creditCode = "914108035664698524";// 企业备案统一社会信用代码
		String pack_Pattern = "DFD311102"; // 电池包型号
		String format_Code = "36";// 规格代码
		String size = "1100*1300*180"; // 尺寸（mm）
		int capacity = 102; // 额定容量(Ah)
		int voltage = 311;// 标称电压（V）
		int quality = 200;// 额定质量（kg）
		int shape = 1;// 包外形
		String cooling_Manner = "自然冷却";// 冷却方式
		String tandem_Manner = "1P14S"; // 包模块串联方式
		int module_Count = 14;// 所含包模块数（个）
		List<Map<String, Object>> modules = new ArrayList<>();// 包含模块明细数组
		Map<String, Object> module = new LinkedHashMap<>();
		module.put("modulel_Pattern", "2P6S");// 模块备案型号
		module.put("count", 14);// 模块个体数量
		modules.add(module);
		int charging_Odds = 1;// 充电倍率（C）
		double power_Density = 180.36;// 功率密度（W/kg）
		int charging_Count = 1000;// 包充放电次数（次）
		int temp_Count = 31;// 包温度探针总数
		double energy_Density = 161.36;// 能量密度（Wh/kg）

		Map<String, Object> packRecord = new LinkedHashMap<>();
		packRecord.put("creditCode", creditCode);
		packRecord.put("pack_Pattern", pack_Pattern);
		packRecord.put("format_Code", format_Code);
		packRecord.put("size", size);
		packRecord.put("capacity", capacity);
		packRecord.put("voltage", voltage);
		packRecord.put("quality", quality);
		packRecord.put("shape", shape);
		packRecord.put("cooling_Manner", cooling_Manner);
		packRecord.put("tandem_Manner", tandem_Manner);
		packRecord.put("module_Count", module_Count);
		packRecord.put("modules", modules);
		packRecord.put("charging_Odds", charging_Odds);
		packRecord.put("power_Density", power_Density);
		packRecord.put("charging_Count", charging_Count);
		packRecord.put("temp_Count", temp_Count);
		packRecord.put("energy_Density", energy_Density);

		return packRecord;
	}

	public Map<String, Object> createModuleRecord() throws Exception {

		String creditCode = "914108035664698524";// 企业备案统一社会信用代码
		String module_Pattern = "2P6S"; // 模块型号
		String format_Code = "44";// 模块规格代码
		int monomer_Count = 12;// 模块所含单体个数
		List<Map<String, Object>> monomerItems = new ArrayList<>();// 包含单体明细数组
		Map<String, Object> monomerItem = new LinkedHashMap<>();
		monomerItem.put("monomer_Pattern", "PSP11102313-51Ah");// 单体备案型号
		monomerItem.put("count", 12);// 对应电池单体个体数量
		monomerItems.add(monomerItem);
		String monomer_Manner = "2P6S"; // 单体串并联方式
		String size = "355*151*108"; // 尺寸（mm）
		int capacity = 102; // 额定容量(Ah)
		double voltage = 22.2;// 标称电压（V）
		double quality = 10.7;// 额定质量（kg）
		int shape = 1;// 模块外形
		int capacity_C3 = 106;// 3 小时率额定容量 C3（Ah）
		double energy_Density = 211.62;// 模块能量密度（Wh/kg）
		int power_Density = 243;// 单体功率密度（W/kg）
		int charging_Odds = 1;// 单体充电倍率（C）
		int charging_Count = 1500;// 单体充放电次数（次）

		Map<String, Object> moduleRecord = new LinkedHashMap<>();
		moduleRecord.put("creditCode", creditCode);
		moduleRecord.put("module_Pattern", module_Pattern);
		moduleRecord.put("format_Code", format_Code);
		moduleRecord.put("monomer_Count", monomer_Count);
		moduleRecord.put("monomerItems", monomerItems);
		moduleRecord.put("monomer_Manner", monomer_Manner);
		moduleRecord.put("size", size);
		moduleRecord.put("capacity", capacity);
		moduleRecord.put("voltage", voltage);
		moduleRecord.put("quality", quality);
		moduleRecord.put("shape", shape);
		moduleRecord.put("capacity_C3", capacity_C3);
		moduleRecord.put("energy_Density", energy_Density);
		moduleRecord.put("power_Density", power_Density);
		moduleRecord.put("charging_Odds", charging_Odds);
		moduleRecord.put("charging_Count", charging_Count);

		return moduleRecord;
	}

	public Map<String, Object> createMonRecord() throws Exception {

		String creditCode = "914108035664698524";// 企业备案统一社会信用代码
		String mon_Pattern = "PSP11102313-51Ah"; // 单体型号
		String format_Code = "13";// 模块规格代码
		int positive = 1;// 正极材料
		int negative = 2;// 负极材料
		String septum_Type = "PE膜";// 隔膜类型
		int electroly_Type = 1;// 电解液类型
		int shape = 1;// 模块外形
		String size = "11.3*101.5*313"; // 尺寸（mm）
		int capacity = 51; // 额定容量(Ah)
		double voltage = 3.7;// 标称电压（V）
		double quality = 0.77;// 额定质量（kg）
		int battery_Type = 5;// 电池类型
		int electroly_Ingredient = 1;// 电解质成分
		int capacity_C3 = 53;// 3 小时率额定容量 C3（Ah）
		double max_Mains = 4.35;// 单体最高允许充电电源（V）
		double energy_Density = 256;// 单体能量密度（Wh/kg）
		int power_Density = 840;// 单体功率密度（W/kg）
		int charging_Odds = 1;// 单体充电倍率（C）
		int charging_Count = 1500;// 单体充放电次数（次）

		Map<String, Object> monRecord = new LinkedHashMap<>();
		monRecord.put("creditCode", creditCode);
		monRecord.put("mon_Pattern", mon_Pattern);
		monRecord.put("format_Code", format_Code);
		monRecord.put("positive", positive);
		monRecord.put("negative", negative);
		monRecord.put("septum_Type", septum_Type);
		monRecord.put("electroly_Type", electroly_Type);
		monRecord.put("quality", quality);
		monRecord.put("shape", shape);
		monRecord.put("size", size);
		monRecord.put("capacity", capacity);
		monRecord.put("voltage", voltage);
		monRecord.put("quality", quality);
		monRecord.put("battery_Type", battery_Type);
		monRecord.put("electroly_Ingredient", electroly_Ingredient);
		monRecord.put("capacity_C3", capacity_C3);
		monRecord.put("max_Mains", max_Mains);
		monRecord.put("energy_Density", energy_Density);
		monRecord.put("power_Density", power_Density);
		monRecord.put("charging_Odds", charging_Odds);
		monRecord.put("charging_Count", charging_Count);

		return monRecord;
	}

	public List<Map<String,Object>> createBatteryPackwuling(String messageNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();
		List<Map<String,Object>> packLists = new ArrayList<>();// 电池包
		try {
			StringBuffer moduleSql = new StringBuffer();
			moduleSql.append(
					"select  mat_no from UDT_IS_Send_ToCustomer where msgHeaderNo='"
					+ messageNo + "'");		
			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			ResultSet moduleRest = modulePrep.executeQuery();

			while (moduleRest.next()) {
				Map<String,Object>packList=new LinkedHashMap<>();
				String creditCode="914108035664698524";//企业备案统一社会信用代码
				String pack_Pattern="DFD311102";//电池包备案型号
				String pack_SpecCode="012PE36";//包厂商规格
				String pack_Code_GB = moduleRest.getString(1);// 电池包编号
				String pack_BarCode_SGMW="";//包条形码（五菱标准）
				String p_Direction="上汽通用五菱汽车股份有限公司";//包流向
				String production_Date=analysisDate1(pack_Code_GB);//生产日期（yyyy-MM-dd）
				int isContain=1;//是否包含模块、单体数据（1：包含，0：不包含）
				StringBuffer barcodeSql = new StringBuffer();
				barcodeSql.append(
						"select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"+ pack_Code_GB +"' and MaterialCode='3000007909'");		
				PreparedStatement barcodePrep = conn.prepareStatement(barcodeSql.toString());
				ResultSet barcodeRest = barcodePrep.executeQuery();
				while(barcodeRest.next()){
				pack_BarCode_SGMW=barcodeRest.getString(1);}
//				String dateCode=production_Date.split("-")[2].toString();
//				String yearAndMonthCode=pack_Code_GB.substring(14, 16);
//				String seqCode=pack_Code_GB.substring(20, 24);
//				String pack_BarCode_SGMW="8484100232363642001"+yearAndMonthCode+dateCode+seqCode;

				packList.put("creditCode", creditCode);
				packList.put("pack_Pattern", pack_Pattern);
				packList.put("pack_SpecCode", pack_SpecCode);
				packList.put("pack_Code_GB", pack_Code_GB);
				packList.put("pack_BarCode_SGMW", pack_BarCode_SGMW);
				packList.put("p_Direction", p_Direction);
				packList.put("production_Date", production_Date);
				packList.put("isContain", isContain);
				packLists.add(packList);
					}
		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return packLists;
	}
	
	public List<Map<String,Object>> createBatteryModulewuling(String packNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();
		List<Map<String,Object>> moduleLists = new ArrayList<>();// 模组
		try {
			StringBuffer moduleSql = new StringBuffer();
			moduleSql.append(
					"select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"
					+ packNo + "' and ISModule=1");		
			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			ResultSet moduleRest = modulePrep.executeQuery();

			while (moduleRest.next()) {
				Map<String,Object>moduleList=new LinkedHashMap<>();
				String creditCode = "914108035664698524";//企业备案统一社会信用代码
				String module_Pattern = "2P6S";//模块备案型号
				String module_SpecCode="012ME44";//模块厂商规格
				String module_Code_GB = moduleRest.getString(1);// 模块编码(国家标准)
				String module_BarCode_SGMW = moduleRest.getString(1);//模块条形码(五菱标准)
				String pack_Code_GB = packNo;// 电池包编号
				String pack_BarCode_SGMW = packNo;//包条形码（五菱标准）
				String m_Direction = "上汽通用五菱汽车股份有限公司";//模块流向
				String production_Date = analysisDate1(module_Code_GB);//生产日期（yyyy-MM-dd）
				moduleList.put("creditCode", creditCode);
				moduleList.put("module_Pattern", module_Pattern);
				moduleList.put("module_SpecCode", module_SpecCode);
				moduleList.put("module_Code_GB", module_Code_GB);
				moduleList.put("module_BarCode_SGMW", module_BarCode_SGMW);
				moduleList.put("pack_Code_GB", pack_Code_GB);
				moduleList.put("pack_BarCode_SGMW", pack_BarCode_SGMW);
				moduleList.put("m_Direction", m_Direction);
				moduleList.put("production_Date", production_Date);
				moduleLists.add(moduleList);
					}
		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return moduleLists;
	}
	
	public List<Map<String,Object>> createBatteryMonwuling(String moduleNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();
		List<Map<String,Object>> moduleLists = new ArrayList<>();// 电芯
		try {
			StringBuffer moduleSql = new StringBuffer();
			//moduleSql.append(
					//"select DxNo from UDT_Process_UnitRefDx where exists (select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"+moduleNo+"' and MaterialNo=UDT_Process_UnitRefDx.UnitNo)");
			moduleSql.append(
					"select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"+moduleNo+"'");
			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			ResultSet moduleRest = modulePrep.executeQuery();

			while (moduleRest.next()) {
				Map<String,Object>moduleList=new LinkedHashMap<>();
				String creditCode = "914108035664698524";//企业备案统一社会信用代码
				String mon_Pattern = "PSP11102313-51Ah";//单体备案型号
				String mon_SpecCode="012CE13";//单体厂商规格
				String mon_Code_GB=moduleRest.getString(1);//单体编码(国家标准)
				String mon_BarCode_SGMW=moduleRest.getString(1);//单体条形码(五菱标准)
				String module_Code_GB = moduleNo;// 模块编码(国家标准)
				String mon_direction = "上汽通用五菱汽车股份有限公司";//单体流向
				String production_Date = analysisDate1(mon_Code_GB);//生产日期（yyyy-MM-dd）
				moduleList.put("creditCode", creditCode);
				moduleList.put("mon_Pattern", mon_Pattern);
				moduleList.put("mon_SpecCode", mon_SpecCode);
				moduleList.put("mon_Code_GB", mon_Code_GB);
				moduleList.put("mon_BarCode_SGMW", mon_BarCode_SGMW);
				moduleList.put("module_Code_GB", module_Code_GB);
				moduleList.put("mon_direction", mon_direction);
				moduleList.put("production_Date", production_Date);
				moduleLists.add(moduleList);
					}
		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return moduleLists;
	}
	
	public List<Map<String,Object>> createBatteryBMSwuling(String packNo) throws Exception {
		Connection conn = ConnectionUtil.getConnection();
		List<Map<String,Object>> BMSLists = new ArrayList<>();// BMS
		try {
			StringBuffer moduleSql = new StringBuffer();
			moduleSql.append(
					"select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"+packNo+"' and MaterialNmae='电池管理系统\\主控\\'");
//			moduleSql.append(
//					"select MaterialNo from UDT_Process_BoxRefParts where BoxCode='"+packNo+"' and MaterialNmae='电池管理系统'");
			PreparedStatement modulePrep = conn.prepareStatement(moduleSql.toString());
			ResultSet moduleRest = modulePrep.executeQuery();

			while (moduleRest.next()) {
				Map<String,Object>BMSList=new LinkedHashMap<>();
				String creditCode = "914108035664698524";//企业备案统一社会信用代码
				String bms_Pattern = "DFD-S01/DFD-H01";//BMS 型号
				String bms_Code=moduleRest.getString(1);//BMS 编号
				String pack_Code_GB=packNo;//所属包编码(国际标准)
				String pack_BarCode_SGMW = packNo;//所属包条形码(五菱标准)
				String production_Date = analysisDate2(bms_Code);//生产日期（yyyy-MM-dd）
				BMSList.put("creditCode", creditCode);
				BMSList.put("bms_Pattern", bms_Pattern);
				BMSList.put("bms_Code", bms_Code);
				BMSList.put("pack_Code_GB", pack_Code_GB);
				BMSList.put("pack_BarCode_SGMW", pack_BarCode_SGMW);
				BMSList.put("production_Date", production_Date);
				BMSLists.add(BMSList);
					}
		} finally {
			ConnectionUtil.closeConnection(conn);
		}
		return BMSLists;
	}

	public String sendSGMW(String url, Map<String, Object> reqHead, Object data, String token, String authPWD)
			throws Exception {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Accept", "application/json");
		try {
			Map<String, Object> requestMap = new LinkedHashMap<String, Object>();
			if (reqHead.get("encryptMode").equals("AES")) {
				requestMap.put("reqHead", reqHead);
				String reqBody = new JsonSerializer().deep(true).serialize(data);
				String enreqContent = encrypt1(reqBody, authPWD);
				Map<String, Object> reqContent = new LinkedHashMap<String, Object>();
				reqContent.put("reqContent", enreqContent);
				requestMap.put("reqBody", reqContent);
			} else {
				requestMap.put("reqHead", reqHead);
				requestMap.put("reqBody", data);
			}
			String response = getRequestSGMW(url, requestMap);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	/**
	 * 相关验证
	 * 
	 * @param Cgdd_Num
	 * @param Send_Base
	 * @param Chn_Sl
	 * @param Box_Num
	 * @param Chn_Bm
	 * @param Chn_Xh
	 * @param Dcb_Bm
	 * @param Dcmk_Bm
	 * @param Dcmk_Xh
	 * @param Dcdt_Bm
	 * @param Dcdt_Xh
	 * @return
	 */
	public String Verification(String Cgdd_Num, String Send_Base, String Chn_Sl, String Box_Num, String Chn_Bm,
			String Chn_Xh, String Dcb_Bm, String Dcb_Xh, String Dcmk_Bm, String Dcmk_Xh, String Dcdt_Bm,
			String Dcdt_Xh) {

		String ReturnPa = "";

		if ("".equals(Cgdd_Num) || Cgdd_Num == null || "".equals(Send_Base) || Send_Base == null || "".equals(Chn_Sl)
				|| Chn_Sl == null /* || "".equals(Chn_Bm)||Chn_Bm==null */
				|| "".equals(Box_Num) || Box_Num == null
				|| /* "".equals(Chn_Xh) ||Chn_Xh==null || */ "".equals(Dcb_Bm) || Dcb_Bm == null || "".equals(Dcb_Xh)
				|| Dcb_Xh == null || "".equals(Dcmk_Bm) || Dcmk_Bm == null || "".equals(Dcmk_Xh) || Dcmk_Xh == null
				|| "".equals(Dcdt_Bm) || Dcdt_Bm == null || "".equals(Dcdt_Xh) || Dcdt_Xh == null) {

			if ("".equals(Cgdd_Num) || Cgdd_Num == null) {
				ReturnPa = "采购订单号不能为空";
				return ReturnPa;
			} else if ("".equals(Send_Base) || Send_Base == null) {
				ReturnPa = "发往基地不能为空";
				return ReturnPa;
			} else if ("".equals(Chn_Sl) || Chn_Sl == null) {
				ReturnPa = "电池包数量不能为空";
				return ReturnPa;
			} /*
				 * else if("".equals(Chn_Bm)||Chn_Bm==null){
				 * ReturnPa="所属储能编号不能为空"; return ReturnPa; }
				 */else if ("".equals(Box_Num) || Box_Num == null) {
				ReturnPa = "箱号不能为空";
				return ReturnPa;
			} /*
				 * else if("".equals(Chn_Xh) ||Chn_Xh==null){
				 * ReturnPa="所属储能型号不能为空"; return ReturnPa; }
				 */else if ("".equals(Dcb_Bm) || Dcb_Bm == null) {
				ReturnPa = "电池包编码不能为空";
				return ReturnPa;
			} else if ("".equals(Dcb_Xh) || Dcb_Xh == null) {
				ReturnPa = "电池包型号不能为空";
				return ReturnPa;
			} else if ("".equals(Dcmk_Bm) || Dcmk_Bm == null) {
				ReturnPa = "电池包编码:" + Dcb_Bm + ";电池模块编码不能为空";
				return ReturnPa;
			} else if ("".equals(Dcmk_Xh) || Dcmk_Xh == null) {
				ReturnPa = "电池包编码:" + Dcb_Bm + ";电池模块型号不能为空";
				return ReturnPa;
			} else if ("".equals(Dcdt_Bm) || Dcdt_Bm == null) {
				ReturnPa = "电池包编码:" + Dcb_Bm + ";电池模块编码:" + Dcmk_Bm + ";电芯编码不能为空";
				return ReturnPa;
			} else if ("".equals(Dcdt_Xh) || Dcdt_Xh == null) {
				ReturnPa = "电池包编码:" + Dcb_Bm + ";电池模块编码:" + Dcmk_Bm + ";电芯型号不能为空";
				return ReturnPa;
			}
			return ReturnPa;
		}

		// 55Ah
		if ("012PE191".equals(Dcb_Bm.substring(0, 8))) {
			if (!"012PE191".equals(Dcb_Xh)) {
				ReturnPa = "电池包对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			} else if (!"2P10S-PSP12161227-55Ah".equals(Dcmk_Xh)) {
				ReturnPa = "电池模块对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			} else if (!"PSP12161227-55Ah".equals(Dcdt_Xh)) {
				ReturnPa = "电池单体对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			}
			return ReturnPa;
		}
		// 46Ah
		if (Dcb_Bm.substring(0, 7) == "012PE28") {
			if (!"012PE191".equals(Dcb_Xh)) {
				ReturnPa = "电池包对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			} else if (!"012ME36".equals(Dcmk_Xh)) {
				ReturnPa = "电池模块对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			} else if (!"012CE05".equals(Dcdt_Xh)) {
				ReturnPa = "电池单体对应厂商规格与厂家规则不符,请联系管理员维护正确的厂商规格";
				return ReturnPa;
			}
			return ReturnPa;
		}
		return ReturnPa;
	}
	
	public String analysisDate1(String code)
	{
		String production_Date="";
		String yearCode=code.substring(14, 15);
		String monthCode=code.substring(15, 16);
		String dayCode=code.substring(16, 17);
		switch(yearCode)
		{
		case"1":yearCode="2011";break;
		case"2":yearCode="2012";break;
		case"3":yearCode="2013";break;
		case"4":yearCode="2014";break;
		case"5":yearCode="2015";break;
		case"6":yearCode="2016";break;
		case"7":yearCode="2017";break;
		case"8":yearCode="2018";break;
		case"9":yearCode="2019";break;
		case"A":yearCode="2020";break;
		case"B":yearCode="2021";break;
		case"C":yearCode="2022";break;
		case"D":yearCode="2023";break;
		case"E":yearCode="2024";break;
		case"F":yearCode="2025";break;
		case"G":yearCode="2026";break;
		case"H":yearCode="2027";break;
		case"J":yearCode="2028";break;
		case"K":yearCode="2029";break;
		case"L":yearCode="2030";break;
		case"M":yearCode="2031";break;
		case"N":yearCode="2032";break;
		case"P":yearCode="2033";break;
		case"R":yearCode="2034";break;
		case"S":yearCode="2035";break;
		case"T":yearCode="2036";break;
		case"V":yearCode="2037";break;
		case"W":yearCode="2038";break;
		case"X":yearCode="2039";break;
		case"Y":yearCode="2040";break;
		}
		switch(monthCode)
		{
		case"1":monthCode="01";break;
		case"2":monthCode="02";break;
		case"3":monthCode="03";break;
		case"4":monthCode="04";break;
		case"5":monthCode="05";break;
		case"6":monthCode="06";break;
		case"7":monthCode="07";break;
		case"8":monthCode="08";break;
		case"9":monthCode="09";break;
		case"A":monthCode="10";break;
		case"B":monthCode="11";break;
		case"C":monthCode="12";break;
		}
		switch(dayCode)
		{
		case"1":dayCode="01";break;
		case"2":dayCode="02";break;
		case"3":dayCode="03";break;
		case"4":dayCode="04";break;
		case"5":dayCode="05";break;
		case"6":dayCode="06";break;
		case"7":dayCode="07";break;
		case"8":dayCode="08";break;
		case"9":dayCode="09";break;
		case"A":dayCode="10";break;
		case"B":dayCode="11";break;
		case"C":dayCode="12";break;
		case"D":dayCode="13";break;
		case"E":dayCode="14";break;
		case"F":dayCode="15";break;
		case"G":dayCode="16";break;
		case"H":dayCode="17";break;
		case"J":dayCode="18";break;
		case"K":dayCode="19";break;
		case"L":dayCode="20";break;
		case"M":dayCode="21";break;
		case"N":dayCode="22";break;
		case"P":dayCode="23";break;
		case"R":dayCode="24";break;
		case"S":dayCode="25";break;
		case"T":dayCode="26";break;
		case"V":dayCode="27";break;
		case"W":dayCode="28";break;
		case"X":dayCode="29";break;
		case"Y":dayCode="30";break;
		case"0":dayCode="31";break;
		}
		production_Date=yearCode+"-"+monthCode+"-"+dayCode;
		return production_Date;
	}
	
	public String analysisDate2(String code)
	{
		String production_Date="";
		String yearCode=code.substring(3, 4);
		String monthCode=code.substring(4, 5);
		String dayCode=code.substring(5, 6);
		switch(yearCode)
		{
		case"1":yearCode="2011";break;
		case"2":yearCode="2012";break;
		case"3":yearCode="2013";break;
		case"4":yearCode="2014";break;
		case"5":yearCode="2015";break;
		case"6":yearCode="2016";break;
		case"7":yearCode="2017";break;
		case"8":yearCode="2018";break;
		case"9":yearCode="2019";break;
		case"A":yearCode="2020";break;
		case"B":yearCode="2021";break;
		case"C":yearCode="2022";break;
		case"D":yearCode="2023";break;
		case"E":yearCode="2024";break;
		case"F":yearCode="2025";break;
		}
		switch(monthCode)
		{
		case"1":monthCode="01";break;
		case"2":monthCode="02";break;
		case"3":monthCode="03";break;
		case"4":monthCode="04";break;
		case"5":monthCode="05";break;
		case"6":monthCode="06";break;
		case"7":monthCode="07";break;
		case"8":monthCode="08";break;
		case"9":monthCode="09";break;
		case"A":monthCode="10";break;
		case"B":monthCode="11";break;
		case"C":monthCode="12";break;
		}
		switch(dayCode)
		{
		case"1":dayCode="01";break;
		case"2":dayCode="02";break;
		case"3":dayCode="03";break;
		case"4":dayCode="04";break;
		case"5":dayCode="05";break;
		case"6":dayCode="06";break;
		case"7":dayCode="07";break;
		case"8":dayCode="08";break;
		case"9":dayCode="09";break;
		case"A":dayCode="10";break;
		case"B":dayCode="11";break;
		case"C":dayCode="12";break;
		case"D":dayCode="13";break;
		case"E":dayCode="14";break;
		case"F":dayCode="15";break;
		case"G":dayCode="16";break;
		case"H":dayCode="17";break;
		case"J":dayCode="18";break;
		case"K":dayCode="19";break;
		case"L":dayCode="20";break;
		case"M":dayCode="21";break;
		case"N":dayCode="22";break;
		case"P":dayCode="23";break;
		case"Q":dayCode="24";break;
		case"R":dayCode="25";break;
		case"S":dayCode="26";break;
		case"T":dayCode="27";break;
		case"U":dayCode="28";break;
		case"V":dayCode="29";break;
		case"W":dayCode="30";break;
		case"X":dayCode="31";break;
		}
		production_Date=yearCode+"-"+monthCode+"-"+dayCode;
		return production_Date;
	}

	public String getRequestqr(String url, String accessToken, Map<String, Object> params) {
		String result = null;
		try {
			CloseableHttpClient client = null;
			CloseableHttpResponse response = null;
			try {
				HttpPost httpPost = new HttpPost(url);
				httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
				String str = new JsonSerializer().deep(true).serialize(params);
				httpPost.setEntity(new StringEntity(str, ContentType.create("text/json", "UTF-8")));

				client = HttpClients.createDefault();
				response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity);
				// System.out.println(result);
			} finally {
				if (response != null) {
					response.close();
				}
				if (client != null) {
					client.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getRequestSGMW(String url, Map<String, Object> params) {
		String result = null;
		try {
			CloseableHttpClient client = null;
			CloseableHttpResponse response = null;
			try {
				HttpPost httpPost = new HttpPost(url);
				httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
				String str = new JsonSerializer().deep(true).serialize(params);
				httpPost.setEntity(new StringEntity(str, ContentType.create("text/json", "UTF-8")));

				client = HttpClients.createDefault();
				response = client.execute(httpPost);
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity);
				// System.out.println(result);
			} finally {
				if (response != null) {
					response.close();
				}
				if (client != null) {
					client.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
